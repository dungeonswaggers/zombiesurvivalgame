package driver;

import controller.GameController;

/**
 * This driver class launches the main game.
 * @author Xhenry Zhang, Mark Wang, Lawrence Lee
 *
 */

/**
 * 3/27/16 Reflections: 
 * Our group managed to get the basic map drawing working. Nothing else. This part
 * took 6 hours to complete among the three of us. Sadly, an hour was spent figuring out how to branch 
 * and merge the file on a mac, the .gitignore wasn't working. Communication was the biggest problem:
 * the group members didn't know what code to keep/leave out after merging. 
 * 
 * Coding the program itself wasn't that bad. The majority of the problems were ones that I solved 
 * previously in past labs, one way or the other. However, the ImageIcon class was horrendous. I had to look at
 * the code for the level editor and the API multiple times to get a sense of what was happening. 
 * Understanding the code and having an active communication are the most important skills in a situation 
 * where there are multiple people working on the same problem. 
 * 
 * Due to time concerns, we couldn't fix a lot of bugs in this game. 
 * @author Xhenry Zhang
 * 
 * Honestly, I felt that we could have done more for this game. However, due to time constraints, our lofty goal of
 * making an amazing game fell flat. I spent many, many hours fixing bugs that exists in this program, but there
 * are still a lot of them :( For example, I coded the part where enemy attacks the surroundings and the character
 * takes damage. Sometimes, the game bugs out a bit and the character takes damage even when the slime isn't nearby.
 * 
 * One thing that I learned a lot in this assignment is how to use git. In the beginning, it was a mess. We as a group
 * didn't really understand how to use git. At first, .gitignore didn't work (stupid Macs...). That was fixed when
 * I copied my teammates' .gitignore. Next, merging was also a big problem, but I learned that merging conflicts can
 * be resolved by removing <<<<< ===== >>>> stuff from the code. Lastly, I learned how to utilize bitbucket and pulling
 * stuff from it. 
 * 
 * Things that could make this game better:
 * More levels! Better monster! Shop items! Character Level progression! Better stats GUI!
 * @Mark Wang
 * 
 * 4/3/2016 Reflections:
 * Some improvements that we as a group have made:
 *  - better looking stats GUI
 *  - better enemy attack system
 *  - character can level up and increase his stats!
 *  - advanced waves of the enemy
 *  - smart AI for the enemy - can show alert when character is near it
 *  
 *  Overall, I am pretty satisfied with how this game turned out. We managed to making GUI look much better than it was before. We made a better AI for
 *  the enemy. Lastly, we were able to get the character to "level up". The only planned features truly missing from the game are the shops and items.
 *  But hey, we can do that after we're done with AP exams.
 *  @Mark Wang
 *  
 */
public class MainGame {
	public static void main(String[] args){
		System.out.println("Welcome to the Survival Arena alpha version! This version contains lots of bugs.");
		System.out.println("Use WASD to move, space to attack. P to open up/close your inventory. (but no items currently");
		System.out.println("Currently enemies can attack but attack animations don't show, waves are infinite.");
		GameController myGame = new GameController();
	}
}
