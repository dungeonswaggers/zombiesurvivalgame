package controller;

import java.awt.event.KeyEvent;

import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;

import model.GameModel;
import view.GameView;
import model.CharacterTile;
import model.EnemyTile;
import model.GameModel;
import model.AttackTile;
import model.BackgroundTile;
import model.CharacterTile;

public class GameController {
	private GameView view;
	private GameModel model;
	boolean PAUSED = false;
	
	public GameController(){
		model = new GameModel(this);
		model.createLevel(1);
		model.updateCharArray();
		view = new GameView(this);
		view.addCustomKeyListener(new MyKeyListener());
		view.getDrawingPanel().repaint();
		view.getDrawingPanel2().repaint();
	}
	
	public ImageIcon pauseMenu(){
		return model.returnInv();
	}
	
	public ImageIcon pauseMenu2(){
		return model.returnInv2();
	}
	
	public ImageIcon statsBar(){
		return model.returnS();
	}
	
	//a few getters to get main character information
		public int getGold(){
			return model.getGold();
		}
		
		public int getAttack(){
			return model.getAttack();
		}
		
		public int getAttackRange(){
			return model.getAttackRange();
		}
		
		public int getDefense(){
			return model.getDefense();
		}
		
		public int getHealth(){
			return model.getHealth();
		}
		
		public int getMaxExp(){
			return model.getMaxExp();
		}
		
		public int getExperience(){
			return model.getExperience();
		}
		
		public int getLevel(){
			return model.getLevel();
		}
		
		public int getArmor(){
			return model.getArmor();
		}
		
		public int getMaxHealth(){
			return model.getMaxHealth();
		}
	
	private class MyKeyListener implements KeyListener{

		@Override
		public void keyTyped(KeyEvent e) {
		
		} 

		@Override
		public void keyPressed(KeyEvent e) {
			int keycode = e.getKeyCode();
			ImageIcon myIcon;
			//Character go up
			CharacterTile character = model.getChar();
			if (keycode == KeyEvent.VK_UP || keycode == KeyEvent.VK_W){
				character.setDirection(1);
				if(model.getBgArray()[character.getRow() - 1][character.getCol()].getPropertyWalkable() == true
						&& model.getEnArray()[character.getRow() - 1][character.getCol()] == null && model.getAtArray()[character.getRow() - 1][character.getCol()] == null){
					character.setRow(character.getRow() - 1);
					myIcon = getModel().createImageIcon("images/main_char_north.png", "North");
					character.setFileName("main_char_north.png");
					character.setImageIcon(myIcon);
					model.updateCharArray();
					view.updateViewChar(view.drawingPanel.getGraphics());
				}
				
			}
			//Character go down
			else if (keycode == KeyEvent.VK_DOWN || keycode == KeyEvent.VK_S){
				character.setDirection(2);
				if(model.getBgArray()[character.getRow() + 1][character.getCol()].getPropertyWalkable() == true
						&& model.getEnArray()[character.getRow() + 1][character.getCol()] == null && model.getAtArray()[character.getRow() + 1][character.getCol()] == null){
					character.setRow(character.getRow() + 1);
					myIcon = getModel().createImageIcon("images/main_char_south.png", "South");
					character.setFileName("main_char_south.png");
					character.setImageIcon(myIcon);
					model.updateCharArray();
					view.updateViewChar(view.drawingPanel.getGraphics());
				}
			}
			//Character go right
			else if (keycode == KeyEvent.VK_RIGHT || keycode == KeyEvent.VK_D){
				character.setDirection(4);
				if(model.getBgArray()[character.getRow()][character.getCol() + 1].getPropertyWalkable() == true
						&& model.getEnArray()[character.getRow()][character.getCol() + 1] == null && model.getAtArray()[character.getRow()][character.getCol() + 1] == null){
					character.setCol(character.getCol() + 1);
					myIcon = getModel().createImageIcon("images/main_char_east.png", "East");
					character.setFileName("main_char_east.png");
					character.setImageIcon(myIcon);
					model.updateCharArray();
					view.updateViewChar(view.drawingPanel.getGraphics());
				}
			}

			//Character go left
			else if (e.getKeyCode() == KeyEvent.VK_LEFT || keycode == KeyEvent.VK_A){
				character.setDirection(3);
				if(model.getBgArray()[character.getRow()][character.getCol() - 1].getPropertyWalkable() == true
						&& model.getEnArray()[character.getRow()][character.getCol() - 1] == null && model.getAtArray()[character.getRow()][character.getCol() - 1] == null){
					character.setCol(character.getCol() - 1);
					myIcon = getModel().createImageIcon("images/main_char_west.png", "West");
					character.setFileName("main_char_west.png");
					character.setImageIcon(myIcon);
					model.updateCharArray();
					view.updateViewChar(view.drawingPanel.getGraphics());
				}
			}
			model.checkPlayerNear();
			//System.out.println("move!" + " Row "+ model.getChar().getRow() + " Col "+ model.getChar().getCol());
		}

		@Override
		public void keyReleased(KeyEvent e) {
			int keycode = e.getKeyCode();
			//Character attack
			if (keycode== KeyEvent.VK_SPACE){
				model.getChar().attack();
			}
			//Pause
			else if (keycode == KeyEvent.VK_P){
				if(!PAUSED){
					PAUSED = true;
				} else {
					PAUSED = false;
				}
				if(PAUSED){
					view.openInv();
				} else {
					view.closeInv();
				}
			}
		}
	}
	
	class BoardListener implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

	}
	
	public GameView getView(){
		return view;
	}
	
	public GameModel getModel(){
		return model;
	}
	
	public BackgroundTile getImageBG(int row, int col){
		BackgroundTile[][] bgArray = model.getBgArray();
		return bgArray[row][col];
	}
	
	public CharacterTile getImageChar(int row, int col){
		CharacterTile[][] chArray = model.getChArray();
		return chArray[row][col];
	}
	
	public EnemyTile getImageEn(int row, int col){
		EnemyTile[][] enArray = model.getEnArray();
		return enArray[row][col];
	}
	
	public AttackTile getImageChAtt(int row, int col){
		AttackTile[][] charAtArray = model.getAtArray();
		return charAtArray[row][col];
	}
	
	public AttackTile getImageEnAt(int row, int col){
		AttackTile[][] enAtArray = model.getEnAtArray();
		return enAtArray[row][col];
	}
}
