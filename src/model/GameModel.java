package model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.ImageIcon;

import controller.GameController;
/**
 *
 * @author Xinyi Zhang, Mark Wang
 * @version Mar 26, 2016
 */

public class GameModel {
	private ImageIcon icon = createImageIcon("images/main_char_south.png", "Cool image!");
	private CharacterTile mainCharacter;
	private int boardWidth = 15;
	private int boardHeight = 15;
	private BackgroundTile[][] bgArray; // Array representing background tiles
	private EnemyTile[][] enArray; // Array representing enemy location
	private EnemyTile[][] enArrayCopy = new EnemyTile[boardHeight][boardWidth]; // A copy of the enemy array, intended for use in updating the array.
	private CharacterTile[][] chArray; // Array representing main character.
	private AttackTile[][] atArray; // Array representing attack animations from the character.
	private AttackTile[][] enAtArray; // Array representing attack animations from the enemy.
	private int level = 1; // level of the current game
	private int wave = 1;
	private int enemyAddAttack = 0;
	private int enemyAddMaxHealth = 0;
	private int enemyAddExp = 5;
	GameController controller;
	
	public GameModel(GameController controller1){
		controller = controller1;
		mainCharacter = new CharacterTile(icon, "main_char.png", 1, 1, 1, controller); // The character
		atArray = new AttackTile[boardHeight][boardWidth];
		chArray = new CharacterTile[boardHeight][boardWidth];
		enAtArray = new AttackTile[boardHeight][boardWidth];
		enArray = new EnemyTile[boardHeight][boardWidth];
		bgArray = new BackgroundTile[boardHeight][boardWidth];
		copyEnArray();
	}
	
	private ImageIcon inv = createImageIcon("images/invent.png", "Cool image!");
	private ImageIcon inv2 = createImageIcon("images/equip.png", "Cool image!");
	private ImageIcon inv3 = createImageIcon("images/realm.png", "Cool image!");
	/**
	 * Creates the data set based on the text document passed to it.
	 * @param fileName - The name of the file.
	 * @throws IOException
	 */
	private void createBoard(String fileName){
		Scanner reader;
		try {
			int rowPos = 0;
			int colPos = 0;
			reader = new Scanner(new File(fileName));
			boardWidth = reader.nextInt();
			boardHeight = reader.nextInt();
			//System.out.println(boardHeight + " " + boardWidth);
			bgArray = new BackgroundTile[boardHeight][boardWidth];
			while(reader.hasNext()){
				String imageFile = reader.next();
				String walkable = reader.next();
				imageFile = removeBr(imageFile);
				walkable = removeBr(walkable);
				//System.out.println(imageFile);
				ImageIcon icon = createImageIcon("images/" + imageFile, "Cool image!");
				BackgroundTile tile = new BackgroundTile(icon, imageFile);
				if(walkable.equals("true")){
					tile.setProertyWalkable(true);
				}else if(walkable.equals("false")){
					tile.setProertyWalkable(false);
				}
				//System.out.println(rowPos + " " + colPos);
				bgArray[rowPos][colPos] = tile;
				if(colPos < boardWidth - 1){
					colPos++;
				}else{
					rowPos++;
					colPos = 0;
				}
				if(rowPos == 15){
					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Updates the character array, with the position of the character, after motion.
	 */
	public void updateCharArray(){
		chArray = new CharacterTile[boardHeight][boardWidth];
		// THEN MAKE THE SQUARE CHAR IS IN THE CHARACTERTILE. 
		chArray[mainCharacter.getRow()][mainCharacter.getCol()] = mainCharacter;
	}
	
	/**
	 * This methods goes through the enemy array and see if there are any changes to the positions of the
	 * enemies.
	 */
	public void updateEnemyArray(){
		copyEnArray();
		enArray = new EnemyTile[boardHeight][boardWidth];
		for(int row = 0; row < boardHeight; row++){
			for(int col = 0; col < boardWidth; col++){
				if(enArrayCopy[row][col] != null){
					EnemyTile enemy = enArrayCopy[row][col];
					int newRow = enemy.getRow();
					int newCol = enemy.getCol();
					enArray[newRow][newCol] = enemy;
				}
			}
		}
	}
	
	/**
	 * This method goes through the "attack array" of character and updates the values there when character declares 
	 * an attack.
	 */
	public void updateChAttArray(AttackTile theAttack){
		atArray = new AttackTile[boardHeight][boardWidth];
		atArray[theAttack.getRow()][theAttack.getCol()] = theAttack;
	}
	
	/**
	 * Updates the attack array of the enemies.
	 */
	public void updateEnAttArray(AttackTile theAttack){
		enAtArray[theAttack.getRow()][theAttack.getCol()] = theAttack;
	}
	
	/**
	 * Deletes the attack tile of the enemies at the given positions.
	 */
	public void deleteEnAttArray(ArrayList<Coordinate> positions){
			for(int i = 0; i < positions.size(); i++){
				int rowPos = positions.get(i).getX();
				int colPos = positions.get(i).getY();
				enAtArray[rowPos][colPos] = null;
			}
	}
	
	/**
	 * Deletes stuff in the attack array.
	 */
	public void deleteChAttArray(){
		atArray = new AttackTile[boardHeight][boardWidth];
	}
	
	/**
	 * Tells all enemies on the board to check if the player is nearby.
	 */
	public void checkPlayerNear(){
		for(int row = 0; row < boardHeight; row++){
			for(int col = 0; col < boardWidth; col++){
				if(enArray[row][col] != null){
					enArray[row][col].setNear(enArray[row][col].checkPlayer());
				}
			}
		}
	}
	
	public ImageIcon returnInv(){
		return inv;
	}
	
	public ImageIcon returnInv2(){
		return inv2;
	}
	
	public ImageIcon returnS(){
		return inv3;
	}
	
	/**
	 * Resets the monsters
	 */
	public void reset(){
		level = 1;
		wave = 1;
		enemyAddAttack = 0;
		enemyAddMaxHealth = 0;
		enemyAddExp = 5;
		for(int row = 0; row < boardHeight; row++){
			for(int col = 0; col < boardWidth; col++){
				if (enArray[row][col] != null){
					enArray[row][col].die();
					copyEnArray();
				}
			}
		}

		controller.getView().clearBg(controller.getView().drawingPanel.getGraphics());
		ImageIcon slimeIcon = createImageIcon("images/enemy_green_slime.png", "This is a slime!");
		EnemyTile begSlime = new EnemyTile(slimeIcon, "enemy_green_slime.png", 
						15, 10, 1, 1, 0, 10, 3, 2, controller, "slime", 1000);
		ImageIcon slimeIcon2 = createImageIcon("images/enemy_red_slime.png", "This is a slime!");
		EnemyTile begSlime2 = new EnemyTile(slimeIcon2, "enemy_red_slime.png", 
						20, 10, 1, 1, 0, 10, 1, 1, controller, "slime", 1000);
		enArray[begSlime.getRow()][begSlime.getCol()] = begSlime;
		enArray[begSlime2.getRow()][begSlime2.getCol()] = begSlime2;
		copyEnArray();
	}
	
	/**
	 * Updates each array based on the current level.
	 * @param lev
	 */
	public void createLevel(int lev){
		level = lev;
		if(level == 1){
			enArray = new EnemyTile[boardHeight][boardWidth];
			createBoard("Level_1");
			ImageIcon slimeIcon = createImageIcon("images/enemy_green_slime.png", "This is a slime!");
			EnemyTile begSlime = new EnemyTile(slimeIcon, "enemy_green_slime.png", 
					15, 10, 1, 1, 0, 10, 3, 2, controller, "slime", 1000);
			ImageIcon slimeIcon2 = createImageIcon("images/enemy_red_slime.png", "This is a slime!");
			EnemyTile begSlime2 = new EnemyTile(slimeIcon2, "enemy_red_slime.png", 
					20, 10, 1, 1, 0, 10, 1, 1, controller, "slime", 1000);
			enArray[begSlime.getRow()][begSlime.getCol()] = begSlime;
			enArray[begSlime2.getRow()][begSlime2.getCol()] = begSlime2;
			copyEnArray();
		}
	}
	
	/**
	 * Updates to a stronger wave
	 */
	public void advancingWave(){
		ImageIcon slimeIcon = createImageIcon("images/enemy_green_slime.png", "This is a slime!");
		ImageIcon slimeIcon2 = createImageIcon("images/enemy_red_slime.png", "This is a slime!");
		
		ArrayList<Coordinate> enCordinates = new ArrayList<Coordinate>();
		
		for(int row = 0; row < boardHeight; row++){
			for(int col = 0; col < boardWidth; col++){
				if(bgArray[row][col].getPropertyWalkable() == true && enArray[row][col] == null && chArray[row][col] == null && atArray[row][col] == null &&
						!bgArray[row][col].getFileName().equals("stairs.png")){
					Coordinate c = new Coordinate(row, col);
					enCordinates.add(c);
					System.out.println(bgArray[row][col].getFileName());
				}
			}
		}
		
		Random rand = new Random();
		int enNumber = wave + 1;
		if (enNumber >= 5){
			enNumber = 4;
			enemyAddAttack ++;
			enemyAddMaxHealth += 5;
			enemyAddExp += 5;
		}
		int alt = 2;
		while (enNumber > 0){
			int pick = rand.nextInt(enCordinates.size());

			int row = enCordinates.get(pick).getX();
			int col = enCordinates.get(pick).getY();
			
			EnemyTile begSlime;
			if (alt % 2 == 0){
				begSlime = new EnemyTile(slimeIcon, "enemy_green_slime.png", 
						15, 10 + enemyAddExp, 1 + enemyAddAttack, 1, 0, 10 + enemyAddMaxHealth, row, col, controller, "slime", 700);
			}
			else {
				begSlime = new EnemyTile(slimeIcon2, "enemy_red_slime.png", 
						15, 10 + enemyAddExp, 1 + enemyAddAttack, 1, 0, 10 + enemyAddMaxHealth, row, col, controller, "slime", 700);
			}
			alt ++;
			
			enCordinates.remove(pick);
			
			enArray[row][col] = begSlime;
			copyEnArray();
			enNumber --;
		}
	}
	
	/**
	 * Temporary method that spawns the next wave. 
	 */
	public void suckySpawnWave(){
		ImageIcon slimeIcon = createImageIcon("images/enemy_green_slime.png", "This is a slime!");
		EnemyTile begSlime = new EnemyTile(slimeIcon, "enemy_green_slime.png", 
				15, 10, 1, 1, 0, 10, 13, 13, controller, "slime", 700);
		ImageIcon slimeIcon2 = createImageIcon("images/enemy_red_slime.png", "This is a slime!");
		EnemyTile begSlime2 = new EnemyTile(slimeIcon2, "enemy_red_slime.png", 
				20, 10, 1, 1, 0, 10, 1, 1, controller, "slime", 700);
		enArray[begSlime.getRow()][begSlime.getCol()] = begSlime;
		enArray[begSlime2.getRow()][begSlime2.getCol()] = begSlime2;
		copyEnArray();
	}
	
	/**
	 * Increases the wave count
	 * @param wave
	 */
	public void incWave(){
		wave ++;
	}
	
	/**
	 * Checks to see if a player attack has hit the enemy.
	 * @param theAttack
	 */
	public void checkHit(AttackTile theAttack){
		for(int row = 0; row < boardHeight; row++){
			for(int col = 0; col < boardWidth; col++){
				if(enArrayCopy[row][col] != null && theAttack.getRow() == row && theAttack.getCol() == col){
					EnemyTile enemy = enArrayCopy[row][col];
					enemy.takeDamage(mainCharacter.getAttack());
					System.out.println("You dealt: " + mainCharacter.getAttack() + " points of damage!");
				}
			}
		}
	}
	
	/**
	 * Checks to see if an enemy hit the player with its attack.
	 * @param positions - Array list of the positions of the enemy attack.
	 */
	public void checkEnHit(ArrayList<Coordinate> positions, EnemyTile enemy){
		for(int i = 0; i < positions.size(); i++){
			if(chArray[positions.get(i).getX()][positions.get(i).getY()] != null){
				mainCharacter.takeDamage(enemy.getAtk() - mainCharacter.getDefense());
				System.out.println("You took " + (enemy.getAtk() - mainCharacter.getDefense()) + " points of damage!");
			}
		}
	}
	
	/**
	 * Returns the number of enemies left alive on the board.
	 * @return
	 */
	public int returnNumEnemy(){
		int numEnemy = 0;
		for(int row = 0; row < boardHeight; row++){
			for(int col = 0; col < boardWidth; col++){
				if(enArray[row][col] != null){
					numEnemy++;
				}
			}
		}
		return numEnemy;
	}
	
	/**
	 * Removes the brackets in the input string.
	 * @param input
	 * @return The string without the brackets.
	 */
	private String removeBr(String input){
		String answer = "";
		for(int i = 0; i < input.length(); i++){
			if(input.charAt(i) != '[' && input.charAt(i) != ']'){
				answer = answer + input.charAt(i);
			}
		}
		return answer;
	}
	
	public ImageIcon createImageIcon(String path, String description) {
		if (path != null) {
			return new ImageIcon(path);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}
	
	//a few getters to get main character information
	public int getGold(){
		return mainCharacter.getGold();
	}
	
	public int getAttack(){
		return mainCharacter.getAttack();
	}
	
	public int getAttackRange(){
		return mainCharacter.getAttackRange();
	}
	
	public int getDefense(){
		return mainCharacter.getDefense();
	}
	
	public int getMaxExp(){
		return mainCharacter.getMaxExp();
	}
	
	public int getHealth(){
		return mainCharacter.getHealth();
	}
	
	public int getExperience(){
		return mainCharacter.getExperience();
	}
	
	public int getLevel(){
		return mainCharacter.getLevel();
	}
	
	public int getArmor(){
		return mainCharacter.getArmor();
	}
	
	public int getMaxHealth(){
		return mainCharacter.getMaxHealth();
	}
	
	public void setExp(int amount){
		mainCharacter.setExperience(getExperience() + amount);
	}
	
	public void setGold(int amount){
		mainCharacter.setGold(getGold() + amount);
	}
	
	/**
	 * Creates a copy of the current enemy array and stores it in enArrayCopy.
	 */
	public void copyEnArray(){
		for(int row = 0; row < boardWidth; row++){
			for(int col = 0; col < boardWidth; col++){
				enArrayCopy[row][col] = enArray[row][col];
			}
		}
	}
	
	public void updateBGArray(int row, int col){
		bgArray = new BackgroundTile[boardWidth][boardHeight];	
	}

	public CharacterTile[][] getChArray(){
		return chArray;
	}
	
	public EnemyTile[][] getEnArray(){
		return enArray;
	}
	
	public AttackTile[][] getEnAtArray(){
		return enAtArray;
	}
	
	public AttackTile[][] getAtArray(){
		return atArray;
	}
	
	public BackgroundTile[][] getBgArray(){
		return bgArray;
	}
	
	public CharacterTile getChar(){
		return mainCharacter;   
	}
}
