package model;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import controller.GameController;
import model.GameModel;
/**
 * A class representing the data contained in the location of the character.
 * @author Xhenry Zhang, Mark Wang
 *
 */
public class CharacterTile extends Tile{
	private GameController controller;
	private boolean dead = false;
	private int gold = 0; // gold of character
	private int attack = 0; // attack points of character
	private int attackRange = 1; // attack range of character
	private int defense = 0; // defense points of character
	private int health = 10; // hit points of character
	private int maxHealth = 0; // max hit points of character
	private int experience = 0; // experience points on character
	private int level = 1; // level of the character
	private int rowPos = 12; // row position in the array
	private int colPos = 7; // column position in the array 
	private int maxExp = 100; // maximum exp before leveling up
	private Timer attFade;

	private int direction = 1; // direction of the character
	/*
	 * 1. North
	 * 2. South
	 * 3. West
	 * 4. East
	 */
	
	private int weapon = 1; 
	/* WEAPON KEY:
	 * 1. Fists +0
	 * 2. Dagger + 1 attack
	 * 3. Sword + 2 attack
	 * 4. Lance + 2 attack + 1 attackRange
	 * 5. Bow + 2 attack + 4 attackRange
	 * 6. Hand Cannon + 5 attack + 4 attackRange
	 * 7. Axe + 2 attack
	 * 8. Bomb (optional)
	 */
	private int armor = 1;

	/*
	 * ARMOR KEY:
	 * 1. Bare Chest
	 * 2. Cloth Armor
	 * 3. Leather Armor
	 * 4. Chain Mail
	 * 5. Leigonaire's Plate
	 * 6. 6 - Pack abs
	 * 7. Golden Armor
	 * 8. Demo Suit
	 * 9. GOD MODE
	 */
	private int boots = 1;
	/*
	 * BOOT KEY:
	 * 1. Leather Boots
	 * 2. Rogue's Boots
	 * 3. Assassin's Boots
	 * 4. Ranger's Boots
	 * 5. Demo Boots
	 *
	 */
	
	Timer regain;
	
	public CharacterTile(ImageIcon icon, String fileName, int weap, int arms, int boot, GameController control) {
		super(icon, fileName);
		weapon = weap;
		armor = arms;
		boots = boot;
		refreshStat();
		controller = control;
		
		regain = new Timer(15000, e->{
			System.out.println("Gained health");
			gainHealth(1);
			controller.getView().getDrawingPanel2().repaint();
		});
		regain.start();
	}

	//a few getters and setters for character stats
	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getAttackRange() {
		return attackRange;
	}

	public void setAttackRange(int attackRange) {
		this.attackRange = attackRange;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
		controller.getView().getDrawingPanel2().repaint();
		if(this.experience >= maxExp){
			levelUp();
			controller.getView().getDrawingPanel2().repaint();
		}
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}
	
	public int getMaxHealth(){
		return maxHealth;
	}
	
	public void setmaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}
	
	public int getMaxExp(){
		return maxExp;
	}
	
	/**
	 * Subtracts from hitpoints and checks if game over.
	 * @param amount - Damage taken
	 */
	public void takeDamage(int amount){
		health = health - amount;
		controller.getView().getDrawingPanel2().repaint();
		if(health <= 0){
			System.out.println("You have died!");
		}
		if(isDead()){
			int response = JOptionPane.showConfirmDialog(controller.getView().getDrawingPanel(), "You lose!\nDo you wish to restart?", "You lose!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (response == JOptionPane.NO_OPTION){
				controller.getView().getWindow().dispose();
			}
			else if (response == JOptionPane.YES_OPTION){
				controller.getModel().reset();
				reset();
				controller.getModel().updateEnemyArray();
				controller.getView().updateViewEnemy(controller.getView().drawingPanel.getGraphics());
				
			}
		}
	}
	
	public void reset(){
		rowPos = 12;
		colPos = 7;
		controller.getModel().updateCharArray();
		controller.getView().updateViewChar(controller.getView().drawingPanel.getGraphics());
		maxHealth = 10;
		health = 10;
		experience = 0;
		maxExp = 100;
		attack = 1;
		defense = 0;
		level = 1;
	}
	public boolean isDead(){
		if (health <= 0){
			return true;
		}
		return false;
	}
	/**
	 * Adds to hitpoints.
	 * @param amount - Hitpoints gained.
	 */
	public void gainHealth(int amount){
		controller.getView().getDrawingPanel2().repaint();
		if (health + amount <= maxHealth){
			health = health + amount;
		}
		else{
			health = maxHealth;
		}
	}

	/**
	 * Refresh the stats of the character based on equips. - dont modify for now
	 */
	public void refreshStat(){
		attack = 1;
		defense = 0;
		maxHealth = 10;
	}
	
	public void setRow(int row){
		rowPos = row;
	}
	
	public void setCol(int col){
		colPos = col;
	}
	
	public int getRow(){
		return rowPos;
	}
	
	public int getCol(){
		return colPos;
	}
	
	public void setDirection(int d){
		direction = d;
	}
	
	/**
	 * Sends out the attack based on the current weapon.
	 */
	public void attack(){
		AttackTile myAttack = null;
		if(weapon == 1){
			switch(direction){
				case 1: 
					ImageIcon attackNorth = controller.getModel().createImageIcon("images/fist_attack_north.png", "Boom.");
					myAttack = new AttackTile(attackNorth, "fist_attack_north.png", rowPos - 1, colPos);
					controller.getModel().updateChAttArray(myAttack); // move later to end of method
					controller.getView().updateViewAttackCh(controller.getView().drawingPanel.getGraphics());
					break;
				case 2: 
					ImageIcon attackSouth = controller.getModel().createImageIcon("images/fist_attack_south.png", "Boom.");
					myAttack = new AttackTile(attackSouth, "fist_attack_south.png", rowPos + 1, colPos);
					controller.getModel().updateChAttArray(myAttack); // move later to end of method
					controller.getView().updateViewAttackCh(controller.getView().drawingPanel.getGraphics());
					break;
				case 3: 
					ImageIcon attackWest = controller.getModel().createImageIcon("images/fist_attack_west.png", "Boom.");
					myAttack = new AttackTile(attackWest, "fist_attack_west.png", rowPos, colPos - 1);
					controller.getModel().updateChAttArray(myAttack); 
					controller.getView().updateViewAttackCh(controller.getView().drawingPanel.getGraphics());
					break;
				case 4: 
					ImageIcon attackEast = controller.getModel().createImageIcon("images/fist_attack_east.png", "Boom.");
					myAttack = new AttackTile(attackEast, "fist_attack_east.png", rowPos, colPos + 1);
					controller.getModel().updateChAttArray(myAttack); 
					controller.getView().updateViewAttackCh(controller.getView().drawingPanel.getGraphics());
					break;
			}
		}
		controller.getModel().updateEnemyArray();
		controller.getModel().checkHit(myAttack);
		attFade = new Timer(50, e ->{
			controller.getView().removeViewAttackCh(controller.getView().drawingPanel.getGraphics());
			controller.getModel().deleteChAttArray();
			attFade.stop();
		}
		);
		attFade.start();
	}
	
	public void levelUp(){
		level++;
		attack++;
		defense++;
		maxHealth += 10;
		health = maxHealth;
		maxExp += 100;
		experience = 0;
		System.out.println("You have reached level " + level + "! Congradulations!");
	}
}
