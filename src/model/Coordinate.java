package model;

/**
 * This class contains the coordinates to pinpoint the row and col positions of a unit on the map.
 * @author Xhenry Zhang
 *
 */
public class Coordinate {
	private int myX;
	private int myY;
	public Coordinate(int x, int y){
		myX = x;
		myY = y;
	}
	
	public int getX(){
		return myX;
	}
	
	public int getY(){
		return myY;
	}
}
