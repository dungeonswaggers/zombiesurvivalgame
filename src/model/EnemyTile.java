package model;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.Timer;
import model.GameModel;

import controller.GameController;
/**
 * This class represents a enemy unit in the game.
 * @author Xhenry Zhang, Mark Wang
 *
 */
public class EnemyTile extends Tile{
	private GameController controller;
	private String myType;
	private int gold;
	private int exp;
	private int attack = 0; // attack points of enemy
	private int attackRange = 0; // attack range of enemy
	private int defense = 0; // defense points of enemy
	private int health = 0; // hit points of enemy
	private int maxHealth = 0; // max hit points of enemy
	private int rowPos;
	private int colPos;
	private int direction = 1;
	private int delay = 750; // delay before moving
	private boolean playerNear = false;
	private Timer attFade;
	private int aggroTime; // if player is near, this is the time before the enemy attacks
	
	private int rowPre = rowPos;
	private int colPre = colPos;
	Timer movement;
	Timer attackMove;
	public EnemyTile(ImageIcon icon, String fileName, int money, int experience, int atk, int atkR,
			int def, int maxHP, int row, int col, GameController controller1, String type, int time) {
		super(icon, fileName);
		myType = type;
		gold = money;
		exp = experience;
		attack = atk;
		defense = def;
		health = maxHP;
		maxHealth = maxHP;
		attackRange = atkR;
		rowPos = row;
		colPos = col;
		aggroTime = time;
		controller = controller1;
		
		movement = new Timer(delay, e ->{
			move();
		}
		);
		movement.start();
		
		attackMove = new Timer(aggroTime, e ->{
			if(playerNear){
				if(health > 0){
					attack();
					attackMove.stop();
				}
			}
		});	
		
	}
	
	//Give gold and EXP to character if enemy dies. Start modifying. For now, just give the main character
	private void giveStuff(){
		if(myType.equals("slime")){
			controller.getModel().setExp(exp);
			controller.getModel().setGold(gold);
		}
		System.out.println("You have slain a slime enemy! You gained " + exp + " experience and " + gold + " gold.");
	}
	
	/**
	 * The enemy attacks, depending on what kind of enemy it is.
	 */
	public void attack(){
		AttackTile myAttack;
		ArrayList<Coordinate> moves = new ArrayList<Coordinate>();
		if(myType.equals("slime")){
	    	moves.add(new Coordinate(rowPos - 1, colPos));
	    	moves.add(new Coordinate(rowPos + 1, colPos));
	    	moves.add(new Coordinate(rowPos, colPos + 1));
	    	moves.add(new Coordinate(rowPos, colPos - 1));
	    	//System.out.println("Im attacking " + rowPos + " " + colPos);
	    	for(int i = 0; i < moves.size(); i++){
	    		if(controller.getModel().getEnAtArray()[moves.get(i).getX()][moves.get(i).getY()] == null){
	    			ImageIcon attack = controller.getModel().createImageIcon("images/slime_attack.png", "Boom.");
		    		myAttack = new AttackTile(attack, "slime_attack.png", moves.get(i).getX(), moves.get(i).getY());
					controller.getModel().updateEnAttArray(myAttack); 
	    		}else{
	    			moves.remove(i);
	    		}
	    	}
	    	controller.getView().updateViewAttackEn(controller.getView().drawingPanel.getGraphics());
	    	controller.getModel().checkEnHit(moves, this);
	    	attFade = new Timer(200, e ->{
	    		controller.getModel().deleteEnAttArray(moves);
				controller.getView().removeViewAttackEn(controller.getView().drawingPanel.getGraphics(), moves);
				attFade.stop();
			}
			);
			attFade.start();
		}
	}
	
	/**
	 * The enemy dies, it is removed from the data board.
	 */
	public void die(){
		controller.getModel().getEnArray()[rowPos][colPos] = null;
		controller.getModel().copyEnArray();
		controller.getView().updateViewEnemy(controller.getView().drawingPanel.getGraphics());
		movement.stop();
		attackMove.stop();
	}
	
	public boolean up(){
		if (controller.getModel().getBgArray()[getRow() - 1][getCol()].getPropertyWalkable() == true && controller.getModel().getChArray()[getRow() - 1][getCol()] == null && controller.getModel().getEnArray()[getRow() - 1][getCol()] == null && controller.getModel().getEnAtArray()[getRow() - 1][getCol()] == null){
			return true;
		}
		return false;
	}
	
	public boolean down(){
		if (controller.getModel().getBgArray()[getRow() + 1][getCol()].getPropertyWalkable() == true && controller.getModel().getChArray()[getRow() + 1][getCol()] == null && controller.getModel().getEnArray()[getRow() + 1][getCol()] == null && controller.getModel().getEnAtArray()[getRow() + 1][getCol()] == null){
			return true;
		}	
		return false;
	}
	
	public boolean left(){
		if (controller.getModel().getBgArray()[getRow()][getCol() - 1].getPropertyWalkable() == true && controller.getModel().getChArray()[getRow()][getCol() - 1] == null && controller.getModel().getEnArray()[getRow()][getCol() - 1] == null && controller.getModel().getEnAtArray()[getRow()][getCol() - 1] == null){
			return true;
		}
		return false;
	}
	
	public boolean right(){
		if (controller.getModel().getBgArray()[getRow()][getCol() + 1].getPropertyWalkable() == true && controller.getModel().getChArray()[getRow()][getCol() + 1] == null && controller.getModel().getEnArray()[getRow()][getCol() + 1] == null && controller.getModel().getEnAtArray()[getRow()][getCol() + 1] == null){
			return true;
		}
		return false;
	}
	
	/**
	 * Moves the monster one square in a random direction.
	 */
	public void move(){
		int charRow = controller.getModel().getChar().getRow();
		int charCol = controller.getModel().getChar().getCol();
		int enemyRow = getRow();
		int enemyCol = getCol();
	
		ArrayList<String> moves = new ArrayList<String>();
//		if (left()){
//			moves.add("left");
//		}
//		if (right()){
//			moves.add("right");
//		}
//		if (up()){
//			moves.add("up");
//		}
//		if (down()){
//			moves.add("down");
//		}
//		
//		// if charRow > enemyRow -> favor right, else favor left
//		// if charCol > enemyCol -> favor down, else favor up
		Random directions = new Random();
//		int rand = directions.nextInt(moves.size());
//		String move = moves.get(rand);
//		if (move.equals("up")){
//			direction = 1;
//			setRow(getRow() - 1);
//		}
//		else if (move.equals("down")){
//			direction = 2;
//			setRow(getRow() + 1);
//		}
//		else if (move.equals("right")){
//			direction = 3;
//			setCol(getCol() + 1);
//		}
//		else if (move.equals("left")){
//			direction = 4;
//		}
		int rand = directions.nextInt(4);
		if (rand == 0 && controller.getModel().getBgArray()[getRow() - 1][getCol()].getPropertyWalkable() == true && controller.getModel().getChArray()[getRow() - 1][getCol()] == null && controller.getModel().getEnArray()[getRow() - 1][getCol()] == null && controller.getModel().getEnAtArray()[getRow() - 1][getCol()] == null){
			direction = 1;
			setRow(getRow() - 1);
		}
		else if (rand == 1 && controller.getModel().getBgArray()[getRow() + 1][getCol()].getPropertyWalkable() == true &&controller.getModel().getChArray()[getRow() + 1][getCol()] == null && controller.getModel().getEnArray()[getRow() + 1][getCol()] == null && controller.getModel().getEnAtArray()[getRow() + 1][getCol()] == null){
			direction = 2;
			setRow(getRow() + 1);
		}
		else if (rand == 2 && controller.getModel().getBgArray()[getRow()][getCol() + 1].getPropertyWalkable() == true && controller.getModel().getChArray()[getRow()][getCol() + 1] == null && controller.getModel().getEnArray()[getRow()][getCol() + 1] == null && controller.getModel().getEnAtArray()[getRow()][getCol() + 1] == null){
			direction = 4;
			setCol(getCol() + 1);
		}
		else if (rand == 3 && controller.getModel().getBgArray()[getRow()][getCol() - 1].getPropertyWalkable() == true && controller.getModel().getChArray()[getRow()][getCol() - 1] == null && controller.getModel().getEnArray()[getRow()][getCol() - 1] == null && controller.getModel().getEnAtArray()[getRow()][getCol() - 1] == null){
			direction = 3;
			setCol(getCol() - 1);
		}
		controller.getModel().updateEnemyArray();
//		System.out.println("View " + controller.getView());
		controller.getView().updateViewEnemy(controller.getView().drawingPanel.getGraphics());
		playerNear = checkPlayer();
	}
	
	/**
	 * Takes damage, checks if damage is fatal.
	 * @param amount
	 */
	public void takeDamage(int amount){
		health = health - amount;
		if(health <= 0){
			giveStuff();
			die();
			// checks to see if all enemies are dead, if so, spawn the next wave
			if(controller.getModel().returnNumEnemy() == 0){
				controller.getModel().incWave();
//				controller.getModel().suckySpawnWave();
				controller.getModel().advancingWave();
			}
		}
	}
	
	public void setRow(int row){
		rowPos = row;
	}
	
	public void setCol(int col){
		colPos = col;
	}
	
	public void setIcon(ImageIcon newIcon){
		super.setImageIcon(newIcon);
	}
	
	public void setFileName(String newName){
		super.setFileName(newName);
	}
	
	public int getRow(){
		return rowPos;
	}
	
	public int getCol(){
		return colPos;
	}
	
	public int getAtk(){
		return attack;
	}
	
	public void setNear(boolean near){
		playerNear = near;
	}
	
	public void setDirection(int d){
		direction = d;
	}
	
	/**
	 * Checks to see if there's a player in the four adjacent squares to the enemy.
	 */
	public boolean checkPlayer(){
    	ArrayList<Coordinate> moves = new ArrayList<Coordinate>();
    	moves.add(new Coordinate(rowPos - 1, colPos));
    	moves.add(new Coordinate(rowPos + 1, colPos));
    	moves.add(new Coordinate(rowPos, colPos + 1));
    	moves.add(new Coordinate(rowPos, colPos - 1));
    	moves.add(new Coordinate(rowPos - 1, colPos - 1));
    	moves.add(new Coordinate(rowPos + 1, colPos + 1));
    	moves.add(new Coordinate(rowPos - 1, colPos + 1));
    	moves.add(new Coordinate(rowPos + 1, colPos - 1));
    	for(int i = 0; i < moves.size(); i++){
    		if(controller.getModel().getChArray()[moves.get(i).getX()][moves.get(i).getY()] != null){
    			//System.out.println("There's a player near me!");
    			ImageIcon myIcon;
    			if(getFileName().equals("enemy_red_slime.png")){
    				myIcon = controller.getModel().createImageIcon("images/enemy_red_slime_alert.png", "This is a slime!");
    				setFileName("enemy_red_slime_alert.png");
    				setIcon(myIcon);
    			}else if(getFileName().equals("enemy_green_slime.png")){
    				myIcon = controller.getModel().createImageIcon("images/enemy_green_slime_alert.png", "This is a slime!");
    				setFileName("enemy_green_slime_alert.png");
    				setIcon(myIcon);
    			}
    			controller.getView().updateViewEnemy(controller.getView().drawingPanel.getGraphics());
    			attackMove.start(); // starts the aggro
    			return true;
    		}
    	}
    	ImageIcon myIcon;
		if(getFileName().equals("enemy_red_slime_alert.png")){
			myIcon = controller.getModel().createImageIcon("images/enemy_red_slime.png", "This is a slime!");
			setFileName("enemy_red_slime.png");
			setIcon(myIcon);
		}else if(getFileName().equals("enemy_green_slime_alert.png")){
			myIcon = controller.getModel().createImageIcon("images/enemy_green_slime.png", "This is a slime!");
			setFileName("enemy_green_slime.png");
			setIcon(myIcon);
		}
		// aggro ends
		if(attackMove.isRunning()){
			attackMove.stop();
		}
		controller.getView().updateViewEnemy(controller.getView().drawingPanel.getGraphics());
    	//System.out.println("There's no player near me!");
    	return false;
	}
}