package model;

import javax.swing.ImageIcon;

/**
 * A class representing the data contained in the location of an attack.
 * @author Xhenry Zhang, Mark Wang
 *
 */
public class AttackTile extends Tile{
	int rowPos;
	int colPos;

	public AttackTile(ImageIcon icon, String fileName, int row, int col) {
		super(icon, fileName);
		// TODO Auto-generated constructor stub
		rowPos = row;
		colPos = col;
	}
	
	public void setRow(int row){
		rowPos = row;
	}
	
	public void setCol(int col){
		colPos = col;
	}
	
	public int getRow(){
		return rowPos;
	}
	
	public int getCol(){
		return colPos;
	}
	
}
