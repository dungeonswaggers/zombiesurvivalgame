package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import controller.GameController;
import model.Coordinate;

public class GameView {
	JFrame window;
	JFrame inventory;
	int GUIHeight = 500;
	int GUIWidth = 650;
	public MyDrawingPanel drawingPanel;
	public MyDrawingPanel2 drawingPanel2;
	public MyDrawingPanel3 drawingPanel3;
	private GameController controls;
	ImageIcon i;
	ImageIcon i2;
	ImageIcon i3;
	JPanel mainPanel;
	
	JLabel gold;
	JLabel attack;
	JLabel AttackRange;
	JLabel Defense;
	JLabel Health;
	JLabel Experience;
	JLabel Level;
	JLabel maxHealth;
	
	
	public GameView(GameController myControl){
		this.controls = myControl;
		JMenuBar menubar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenu helpMenu = new JMenu("Edit");
		JMenuItem openItem = new JMenuItem("New Game", 'o');
		JMenuItem saveItem = new JMenuItem("Exit", 's');
		JMenuItem pause = new JMenuItem("Pause", 'p');
		JMenuItem howTo = new JMenuItem("How to Play", 'h');
		JMenuItem about = new JMenuItem("About", 'a');
		fileMenu.add(openItem);
		fileMenu.add(saveItem);
		fileMenu.add(pause);
		helpMenu.add(howTo);
		helpMenu.add(about);
		menubar.add(fileMenu);
		menubar.add(helpMenu);
		i = controls.pauseMenu();
		i2 = controls.pauseMenu2();
		i3 = controls.statsBar();
		
		// Create Java Window
		window = new JFrame("Dungeon Swaggers");
		window.setBounds(100, 100, GUIWidth, GUIHeight);
		//window.getContentPane().setLayout(null);
		window.setResizable(false);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setBackground(Color.BLACK);

		drawingPanel = new MyDrawingPanel();
		drawingPanel.setBounds(20, 20, 407, 407);

		drawingPanel2 = new MyDrawingPanel2();
		drawingPanel2.setBounds(440, 20, 200, 400);
		drawingPanel2.setBorder(BorderFactory.createEtchedBorder());

		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		
		mainPanel.add(drawingPanel);
		mainPanel.add(drawingPanel2);
		
		window.getContentPane().add(mainPanel);
		window.setJMenuBar(menubar);
		// Let there be light
		window.setVisible(true);
		
		inventory = new JFrame("Inventory");
		inventory.setBounds(100, 100, 600, 550);
		inventory.setResizable(false);
		inventory.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		drawingPanel3 = new MyDrawingPanel3();
		drawingPanel3.setBounds(0, 0, 600, 600);
		JPanel mainPanel2 = new JPanel();
		mainPanel2.setLayout(null);
		mainPanel2.add(drawingPanel3);
		inventory.getContentPane().add(mainPanel2);
		inventory.setVisible(false);
		
		Font f = new Font("Arial", Font.BOLD, 11);
		
		attack = new JLabel("" + controls.getAttack());
		attack.setForeground(Color.gray);
		attack.setFont(f);
		attack.setBounds(0, 20, 10, 10);
		drawingPanel2.add(attack);
		
		gold = new JLabel("" + controls.getGold());
		gold.setForeground(Color.gray);
		gold.setFont(f);
		gold.setBounds(0, 20, 10, 10);
		drawingPanel2.add(gold);
		
		Defense = new JLabel("" + controls.getDefense());
		Defense.setForeground(Color.gray);
		Defense.setFont(f);
		Defense.setBounds(0, 20, 10, 10);
		drawingPanel2.add(Defense);
		
		AttackRange = new JLabel("" + controls.getAttackRange());
		AttackRange.setForeground(Color.gray);
		AttackRange.setFont(f);
		AttackRange.setBounds(0, 20, 10, 10);
		drawingPanel2.add(AttackRange);
		
		Level = new JLabel("Lvl " + controls.getLevel());
		Level.setForeground(Color.WHITE);
		Level.setFont(f);
		Level.setBounds(0, 20, 10, 10);
		drawingPanel2.add(Level);
		
		maxHealth = new JLabel("HP " + controls.getHealth() + "/" + controls.getMaxHealth());
		maxHealth.setForeground(Color.WHITE);
		maxHealth.setFont(f);
		maxHealth.setBounds(0, 20, 10, 10);
		drawingPanel2.add(maxHealth);
		
		/*
		attack = new JLabel("Attack: " + controls.getAttack());
		attack.setBounds(10, 30, 100, 25);
		drawingPanel2.add(attack);
		
		AttackRange = new JLabel("AttackRange: " + controls.getAttackRange());
		AttackRange.setBounds(10, 50, 100, 25);
		drawingPanel2.add(AttackRange);
		
		Defense = new JLabel("Defense: " + controls.getDefense());
		Defense.setBounds(10, 70, 100, 25);
		drawingPanel2.add(Defense);
		
		Health = new JLabel("Health: " + controls.getHealth() + "/" + controls.getMaxHealth());
		Health.setBounds(10, 90, 100, 25);
		drawingPanel2.add(Health);
		
		Experience = new JLabel("Experience: " + controls.getExperience());
		Experience.setBounds(10, 110, 100, 25);
		drawingPanel2.add(Experience);
		
		Level = new JLabel("Level: " + controls.getLevel());
		Level.setBounds(10, 130, 100, 25);
		drawingPanel2.add(Level);*/
	}
	
	/**
	 * Updates the view based on character array.
	 * @param g
	 */
	public void updateViewChar(Graphics g){
		int hOffset = -1; // these values determine the offsets from the selected position
    	int vOffset = -1;
    	int charRow = 0; // the row that the character is in
    	int charCol = 0; // the col that the character is in
    	
    	// Redrawing the character
		for(int row = 0; row < 15; row++){
			for(int col = 0; col < 15; col++){
				if(controls.getImageChar(row, col) != null){
					charRow = row;
					charCol = col;
					//System.out.println(charRow + " " + charCol);
					ImageIcon image = controls.getImageChar(row, col).getImageIcon();
					Image img = image.getImage();
					Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
					ImageIcon newIcon = new ImageIcon(newImg);
					newIcon.paintIcon(drawingPanel, g, 27 * col, 27 * row);
					break;
				}
			}
		}
		
		// Redrawing background tiles around the character.
		for(int x = hOffset; x <= 1; x++){
    		for(int y = vOffset; y <= 1; y++){
    			if(!(x == 0 && y == 0)){
    				ImageIcon image = controls.getImageBG(charRow + y, charCol + x).getImageIcon();
    				Image img = image.getImage();
    				Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
    				ImageIcon newIcon = new ImageIcon(newImg);
    				newIcon.paintIcon(drawingPanel, g, 27 * (charCol + x), 27 * (charRow + y));
    				if(controls.getImageEn(charRow + y, charCol + x) != null){
    					ImageIcon image2 = controls.getImageEn(charRow + y, charCol + x).getImageIcon();
    					Image img2 = image2.getImage();
    					Image newImg2 = img2.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
    					ImageIcon newIcon2 = new ImageIcon(newImg2);
    					newIcon2.paintIcon(drawingPanel, g, 27 * (charCol + x), 27 * (charRow + y));
    				}
    				if(controls.getImageEnAt(charRow + y, charCol + x) != null){
    					ImageIcon image2 = controls.getImageEnAt(charRow + y, charCol + x).getImageIcon();
    					Image img2 = image2.getImage();
    					Image newImg2 = img2.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
    					ImageIcon newIcon2 = new ImageIcon(newImg2);
    					newIcon2.paintIcon(drawingPanel, g, 27 * (charCol + x), 27 * (charRow + y));
    				}
    			}
    		}
    	}
		
		
	}
	
	public void clearBg(Graphics g){
		for (int row = 0; row < 15; row++){
			for (int col = 0; col < 15; col++){
				ImageIcon image = controls.getImageBG(row, col).getImageIcon();
				Image img = image.getImage();
				Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
				ImageIcon newIcon = new ImageIcon(newImg);
				newIcon.paintIcon(drawingPanel, g, 27 * (col), 27 * (row));
			}
		}
		
	}
	/**
	 * Updates the view according to the enemy array. 
	 * @param g
	 */
	public void updateViewEnemy(Graphics g){
		drawingPanel2.repaint();
		int hOffset = -1; // these values determine the offsets from the selected position
    	int vOffset = -1;
    	int myRow = 0; // the row that the character is in
    	int myCol = 0; // the col that the character is in
    	ArrayList<Coordinate> enemyPosition = new ArrayList<Coordinate>(); // creates an array list, soon to
    	// be added with positions of the enemies
    
		// Redrawing the enemies.
		for(int row = 0; row < 15; row++){
			for(int col = 0; col < 15; col++){
				if(controls.getImageEn(row, col) != null){
					Coordinate myLocation = new Coordinate(row, col);
					enemyPosition.add(myLocation);
					ImageIcon image = controls.getImageEn(row, col).getImageIcon();
					Image img = image.getImage();
					Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
					ImageIcon newIcon = new ImageIcon(newImg);
					newIcon.paintIcon(drawingPanel, g, 27 * col, 27 * row);
				}
			}
		}
		
		// Clears the squares around the enemy, revert to background.
		for(int count = 0; count < enemyPosition.size(); count++){
			for(int x = hOffset; x <= 1; x++){
	    		for(int y = vOffset; y <= 1; y++){
	    			myRow = enemyPosition.get(count).getX() + y;
	    			myCol = enemyPosition.get(count).getY() + x;
	    			if(!(x == 0 && y == 0) && controls.getImageEn(myRow, myCol) == null && controls.getImageChar(myRow, myCol) == null && controls.getImageEnAt(myRow, myCol) == null){
	    				ImageIcon image = controls.getImageBG(myRow, myCol).getImageIcon();
	    				Image img = image.getImage();
	    				Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
	    				ImageIcon newIcon = new ImageIcon(newImg);
	    				newIcon.paintIcon(drawingPanel, g, 27 * (myCol), 27 * (myRow));
	    			}else if(x == 0 && y == 0){
	    				ImageIcon image2 = controls.getImageBG(myRow, myCol).getImageIcon();
	    				Image img2 = image2.getImage();
	    				Image newImg2 = img2.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
	    				ImageIcon newIcon2 = new ImageIcon(newImg2);
	    				newIcon2.paintIcon(drawingPanel, g, 27 * (myCol), 27 * (myRow));
						ImageIcon image = controls.getImageEn(myRow, myCol).getImageIcon();
						Image img = image.getImage();
						Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
						ImageIcon newIcon = new ImageIcon(newImg);
						newIcon.paintIcon(drawingPanel, g, 27 * myCol, 27 * myRow);
	    			}
	    		}
	    	}
		}
	}
	
	/**
	 * Draws the attacks on the board. - start working from here.
	 */
	public void updateViewAttackCh(Graphics g){
		for(int row = 0; row < 15; row++){
			for(int col = 0; col < 15; col++){
				if(controls.getImageChAtt(row, col) != null){
					ImageIcon image = controls.getImageChAtt(row, col).getImageIcon();
					Image img = image.getImage();
					Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
					ImageIcon newIcon = new ImageIcon(newImg);
					newIcon.paintIcon(drawingPanel, g, 27 * col, 27 * row);
					break;
				}
			}
		}
	}
	
	/**
	 * Draws the enemy attacks on the board. - start working from here.
	 */
	public void updateViewAttackEn(Graphics g){
		for(int row = 0; row < 15; row++){
			for(int col = 0; col < 15; col++){
				if(controls.getImageEnAt(row, col) != null){
					ImageIcon image = controls.getImageEnAt(row, col).getImageIcon();
					Image img = image.getImage();
					Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
					ImageIcon newIcon = new ImageIcon(newImg);
					newIcon.paintIcon(drawingPanel, g, 27 * col, 27 * row);
				}
			}
		}
	}
	
	/**
	 * Removes the enemy attacks on the board.
	 */
	public void removeViewAttackEn(Graphics g, ArrayList<Coordinate> positions){
		for(int i = 0; i < positions.size(); i++){
			
			int rowPos = positions.get(i).getX();
			int colPos = positions.get(i).getY();
			// If it is an enemy, replace the image with enemy image and background.
			if(controls.getImageEn(rowPos, colPos) != null){
				ImageIcon image2 = controls.getImageBG(rowPos, colPos).getImageIcon();
				Image img2 = image2.getImage();
				Image newImg2 = img2.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
				ImageIcon newIcon2 = new ImageIcon(newImg2);
				newIcon2.paintIcon(drawingPanel, g, 27 * colPos, 27 * rowPos);
				ImageIcon image = controls.getImageEn(rowPos, colPos).getImageIcon();
				Image img = image.getImage();
				Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
				ImageIcon newIcon = new ImageIcon(newImg);
				newIcon.paintIcon(drawingPanel, g, 27 * colPos, 27 * rowPos);
				// If it is a character, replace the image with character image and background.
			}else if(controls.getImageChar(rowPos, colPos) != null){
				ImageIcon image2 = controls.getImageBG(rowPos, colPos).getImageIcon();
				Image img2 = image2.getImage();
				Image newImg2 = img2.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
				ImageIcon newIcon2 = new ImageIcon(newImg2);
				newIcon2.paintIcon(drawingPanel, g, 27 * colPos, 27 * rowPos);
				ImageIcon image = controls.getImageChar(rowPos, colPos).getImageIcon();
				Image img = image.getImage();
				Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
				ImageIcon newIcon = new ImageIcon(newImg);
				newIcon.paintIcon(drawingPanel, g, 27 * colPos, 27 * rowPos);
			}else{
				ImageIcon image = controls.getImageBG(rowPos, colPos).getImageIcon();
				Image img = image.getImage();
				Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
				ImageIcon newIcon = new ImageIcon(newImg);
				newIcon.paintIcon(drawingPanel, g, 27 * colPos, 27 * rowPos);
			}
		}
		for(int row = 0; row < 15; row++){
			for(int col = 0; col < 15; col++){
				if(controls.getImageEnAt(row, col) != null){
					ImageIcon image = controls.getImageEnAt(row, col).getImageIcon();
					Image img = image.getImage();
					Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
					ImageIcon newIcon = new ImageIcon(newImg);
					newIcon.paintIcon(drawingPanel, g, 27 * col, 27 * row);
				}
			}
		}
	}
	
	/**
	 * Removes the attack animation from the screen.
	 */
	public void removeViewAttackCh(Graphics g){
		for(int row = 0; row < 15; row++){
			for(int col = 0; col < 15; col++){
				if(controls.getImageChAtt(row, col) != null){
					// If it is an enemy, replace the image with enemy image and background.
					if(controls.getImageEn(row, col) != null){
						ImageIcon image2 = controls.getImageBG(row, col).getImageIcon();
						Image img2 = image2.getImage();
						Image newImg2 = img2.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
						ImageIcon newIcon2 = new ImageIcon(newImg2);
						newIcon2.paintIcon(drawingPanel, g, 27 * col, 27 * row);
						ImageIcon image = controls.getImageEn(row, col).getImageIcon();
						Image img = image.getImage();
						Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
						ImageIcon newIcon = new ImageIcon(newImg);
						newIcon.paintIcon(drawingPanel, g, 27 * col, 27 * row);
						break;
					}else{
						ImageIcon image = controls.getImageBG(row, col).getImageIcon();
						Image img = image.getImage();
						Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
						ImageIcon newIcon = new ImageIcon(newImg);
						newIcon.paintIcon(drawingPanel, g, 27 * col, 27 * row);
						break;
					}
					
				}
			}
		}
	}
	
	public void openInv(){
		inventory.setVisible(true);
	}
	
	public void closeInv(){
		//System.out.println("weeee");
		inventory.setVisible(false);
	}
	
	public void addCustomKeyListener(KeyListener x){
		window.addKeyListener(x);
		inventory.addKeyListener(x);
	}
	
	public class MyDrawingPanel extends JPanel {

		// Not required, but gets rid of the serialVersionUID warning.  Google it, if desired.
		static final long serialVersionUID = 1234567890L;

		public void paintComponent(Graphics g) {

			g.setColor(Color.lightGray);
			for (int x = 0; x < this.getWidth(); x += 27)
				g.drawLine(x, 0, x, this.getHeight());

			for (int y = 0; y < this.getHeight(); y += 27)
				g.drawLine(0, y, this.getWidth(), y);
			
			// ADDING BACKGROUND TO GRAPHICS
			for(int row = 0; row < 15; row++){
				for(int col = 0; col < 15; col++){
					ImageIcon image = controls.getImageBG(row, col).getImageIcon();
					Image img = image.getImage();
					Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
					ImageIcon newIcon = new ImageIcon(newImg);
					newIcon.paintIcon(this, g, 27 * col, 27 * row);
				}
			}
			
			// ADDING CHARACTER TO GRAPHICS
			for(int row = 0; row < 15; row++){
				for(int col = 0; col < 15; col++){
					if(controls.getImageChar(row, col) != null){
						ImageIcon image = controls.getImageChar(row, col).getImageIcon();
						Image img = image.getImage();
						Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
						ImageIcon newIcon = new ImageIcon(newImg);
						newIcon.paintIcon(this, g, 27 * col, 27 * row);
						break;
					}
				}
			}
			
			// ADDING ENEMIES TO GRAPHICS
			for(int row = 0; row < 15; row++){
				for(int col = 0; col < 15; col++){
					if(controls.getImageEn(row, col) != null){
						ImageIcon image = controls.getImageEn(row, col).getImageIcon();
						Image img = image.getImage();
						Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
						ImageIcon newIcon = new ImageIcon(newImg);
						newIcon.paintIcon(this, g, 27 * col, 27 * row);
					}
				}
			}
		}
		
		public void updateBg(Graphics g){
			for(int row = 0; row < 15; row++){
				for(int col = 0; col < 15; col++){
					ImageIcon image = controls.getImageBG(row, col).getImageIcon();
					Image img = image.getImage();
					Image newImg = img.getScaledInstance(27, 27, java.awt.Image.SCALE_SMOOTH);
					ImageIcon newIcon = new ImageIcon(newImg);
					newIcon.paintIcon(this, g, 27 * col, 27 * row);
				}
			}
		}
	}
	
	private class MyDrawingPanel2 extends JPanel {

		// Not required, but gets rid of the serialVersionUID warning.  Google it, if desired.
		static final long serialVersionUID = 1234567890L;

		public void paintComponent(Graphics g) {

			g.setColor(Color.WHITE);
			g.fillRect(2, 2, this.getWidth()-2, this.getHeight()-2);

			g.setColor(Color.lightGray);
			/*for (int x = 0; x < this.getWidth(); x += 40)
				g.drawLine(x, 0, x, this.getHeight());

			for (int y = 0; y < this.getHeight(); y += 40)
				g.drawLine(0, y, this.getWidth(), y);*/
			
			Image img = i3.getImage();
			Image newImg = img.getScaledInstance(this.getWidth(), this.getHeight(), java.awt.Image.SCALE_SMOOTH);
			ImageIcon newIcon = new ImageIcon(newImg);
			newIcon.paintIcon(this, g, 0, 0);
			
			
			g.setColor(Color.GRAY);
            g.fillRect(14, 172, 174, 15);
            
            g.setColor(Color.RED);
            g.fillRect(14, 172, ((controls.getHealth() * 174))/(controls.getMaxHealth()), 15);
            g.setColor(Color.lightGray);
            
            g.setColor(Color.GRAY);
            g.fillRect(14, 150, 174, 15);
            
            g.setColor(Color.GREEN);
            g.fillRect(14, 150, ((controls.getExperience() * 174))/controls.getMaxExp(), 15);
            g.setColor(Color.lightGray);
 
    		attack.setBounds(50, 308, 30, 10);
    		attack.setText("" + controls.getAttack());
    		
    		gold.setBounds(64, 333, 30, 10);
			gold.setText("" + controls.getGold());
			
			Defense.setBounds(145, 308, 30, 10);
			Defense.setText("" + controls.getDefense());
			
			AttackRange.setBounds(169, 333, 30, 10);
			AttackRange.setText("" + controls.getAttackRange());
			
			Level.setBounds(15, 153, 30, 10);
			Level.setText("Lvl " + controls.getLevel());
			
			maxHealth.setBounds(15, 175, 60, 10);
            maxHealth.setText("HP " + controls.getHealth() + "/" + controls.getMaxHealth());
            
		}
	}
	
	private class MyDrawingPanel3 extends JPanel {

		// Not required, but gets rid of the serialVersionUID warning.  Google it, if desired.
		static final long serialVersionUID = 1234567890L;

		public void paintComponent(Graphics g) {
			Image img = i.getImage();
			Image newImg = img.getScaledInstance(350, 500, java.awt.Image.SCALE_SMOOTH);
			ImageIcon newIcon = new ImageIcon(newImg);
			newIcon.paintIcon(this, g, 0, 0);
			//System.out.println("hii");
			
			Image img2 = i2.getImage();
			Image newImg2 = img2.getScaledInstance(250, 500, java.awt.Image.SCALE_SMOOTH);
			ImageIcon newIcon2 = new ImageIcon(newImg2);
			newIcon2.paintIcon(this, g, 350, 0);
			
		}
	}
	
	public JPanel getDrawingPanel(){
		return drawingPanel;
	}
	
	public JPanel getDrawingPanel2(){
		return drawingPanel2;
	}
	
	public JFrame getWindow(){
		return window;
	}
}


